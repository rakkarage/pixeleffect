# **PixelEffect** ✨

<img align="right" src="icon.png">

Pixel-perfect animated path effect.

- 📦 <http://henrysoftware.itch.io/godot-pixel-effect>
- 🌐 <http://rakkarage.github.io/PixelEffect>
- 📃 <http://guthub.com/rakkarage/PixelEffect>

[![.github/workflows/compress.yml](https://github.com/rakkarage/PixelEffect/actions/workflows/compress.yml/badge.svg)](https://github.com/rakkarage/PixelEffect/actions/workflows/compress.yml)
[![.github/workflows/deploy.yml](https://github.com/rakkarage/PixelEffect/actions/workflows/deploy.yml/badge.svg)](https://github.com/rakkarage/PixelEffect/actions/workflows/deploy.yml)
